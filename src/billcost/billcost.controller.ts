import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { BillcostsService } from './billcost.service';
import { CreateBillcostDto } from './dto/create-billcost.dto';
import { UpdateBillcostDto } from './dto/update-billcost.dto';

@Controller('billcosts')
export class BillcostsController {
  constructor(private readonly billcostsService: BillcostsService) {}
  // Create
  @Post()
  create(@Body() createBillcostDto: CreateBillcostDto) {
    return this.billcostsService.create(createBillcostDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.billcostsService.findAll();
  }
  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.billcostsService.findOne(+id);
  }
  // Partial Update
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBillcostDto: UpdateBillcostDto,
  ) {
    return this.billcostsService.update(+id, updateBillcostDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.billcostsService.remove(+id);
  }
}
