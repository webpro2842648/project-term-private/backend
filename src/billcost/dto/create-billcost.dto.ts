import { IsNotEmpty, IsNumber, IsString, Length, Min } from 'class-validator';

export class CreateBillcostDto {
  @IsNotEmpty()
  type: 'Electricity bill' | 'Water bill' | 'Rent bill' | 'other';

  // Change to Date and remove IsDate
  date: Date;

  @IsString()
  time: string;

  @IsNumber()
  @Min(0, { message: 'Total must be greater than or equal to 0' })
  total: number;
}
