import { PartialType } from '@nestjs/swagger';
import { CreateBillcostDto } from './create-billcost.dto';

export class UpdateBillcostDto extends PartialType(CreateBillcostDto) {}
