import { Test, TestingModule } from '@nestjs/testing';
import { BillcostsService } from './billcost.service';

describe('BillcostsService', () => {
  let service: BillcostsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BillcostsService],
    }).compile();

    service = module.get<BillcostsService>(BillcostsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
