import { Module } from '@nestjs/common';
import { BillcostsService } from './billcost.service';
import { BillcostsController } from './billcost.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Billcost } from './entities/billcost.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Billcost])],
  controllers: [BillcostsController],
  providers: [BillcostsService],
})
export class BillcostsModule {}
