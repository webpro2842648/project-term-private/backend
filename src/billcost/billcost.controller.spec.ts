import { Test, TestingModule } from '@nestjs/testing';
import { BillcostsController } from './billcost.controller';
import { BillcostsService } from './billcost.service';

describe('BillcostsController', () => {
  let controller: BillcostsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BillcostsController],
      providers: [BillcostsService],
    }).compile();

    controller = module.get<BillcostsController>(BillcostsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
