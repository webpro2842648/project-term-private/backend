import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Billcost {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: 'Electricity bill' | 'Water bill' | 'Rent bill' | 'other';

  @Column({ type: 'date' }) // Change to 'date' type
  date: Date;

  @Column()
  time: string;

  @Column()
  total: number;
}
