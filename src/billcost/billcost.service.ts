import { Injectable } from '@nestjs/common';
import { CreateBillcostDto } from './dto/create-billcost.dto';
import { UpdateBillcostDto } from './dto/update-billcost.dto';
import { Billcost } from './entities/billcost.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
@Injectable()
export class BillcostsService {
  constructor(
    @InjectRepository(Billcost)
    private billcostsRepository: Repository<Billcost>,
  ) {}
  // create(createBillcostDto: CreateBillcostDto): Promise<Billcost> {
  //   return this.billcostsRepository.save(createBillcostDto);
  // }
  async create(createBillcostDto: CreateBillcostDto): Promise<Billcost> {
    const { date, ...rest } = createBillcostDto;
    const newBillcost = this.billcostsRepository.create({
      date: new Date(date),
      ...rest,
    });
    return this.billcostsRepository.save(newBillcost);
  }
  
  async findAll(): Promise<Billcost[]> {
    return this.billcostsRepository.find();
  }
  
  findOne(id: number) {
    return this.billcostsRepository.findOneBy({ id });
  }
  async update(id: number, updateBillcostDto: UpdateBillcostDto) {
    await this.billcostsRepository.update(id, updateBillcostDto);
    const billcost = await this.billcostsRepository.findOneBy({ id });
    return billcost;
  }

  async remove(id: number) {
    const deleteBillcost = await this.billcostsRepository.findOneBy({ id });
    return this.billcostsRepository.remove(deleteBillcost);
  }
}
