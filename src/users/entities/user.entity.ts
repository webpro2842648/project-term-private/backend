import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  email: string;
  @Column()
  password: string;
  @Column()
  fullName: string;
  @Column()
  // roles: ('admin' | 'user')[];
  gender: 'male' | 'female' | 'others';
}
